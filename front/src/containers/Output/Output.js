import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Box, Button, CardMedia, Grid, makeStyles, Paper, Typography} from "@material-ui/core";
import {apiURL} from "../../config";
import {deleteOutput, fetchOutput} from "../../store/actions/outputsActions";

const useStyles = makeStyles(theme => ({
    media: {
        height: 0,
        paddingTop: '56.25%',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        backgroundBlendMode: 'darken',
    },
}));
const Output = ({match, history}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const {output} = useSelector(state => state.outputs);
    const {user} = useSelector(state => state.users);


    useEffect(() => {
        dispatch(fetchOutput(match.params.id));
    }, [dispatch, match.params.id]);

    const removeOutput = () => {
        dispatch(deleteOutput(match.params.id, history))
    }


    return output && (
        <Paper component={Box} p={4}>
            <Paper component={Box} p={3}>
                <Grid item>
                    <CardMedia className={classes.media}
                               image={apiURL + '/' + output.image}/>
                    <Grid container direction="column" spacing={3}>
                        <Grid item>
                            <Grid container justifyContent="space-between">
                                <Grid item>
                                    <Typography variant='h4'>
                                        Title: {output.title}
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Typography variant='subtitle2'>
                                        Phone number: {output.author.phoneNumber}
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Typography variant='body1'>
                                Author: {output.author.displayName}
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant='body1'>
                                Category: {output.category.title}
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant='body1'>
                                Info: {output.description}
                            </Typography>
                        </Grid>
                        {
                            output?.author?._id === user?._id && (
                                <Grid item>
                                    <Button onClick={removeOutput} color="primary" variant="outlined">Delete</Button>
                                </Grid>)
                        }
                    </Grid>
                </Grid>
            </Paper>
        </Paper>
    );
};

export default Output;