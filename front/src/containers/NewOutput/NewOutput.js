import React from 'react';
import {Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import Form from "../../components/Form/Form";
import {toast} from "react-toastify";
import {createOutput} from "../../store/actions/outputsActions";

const NewOutput = ({history}) => {
    const dispatch = useDispatch();
    const {categories} = useSelector(state => state.categories)


    const onSubmit = async data => {
        await dispatch(createOutput(data));
        history.replace('/');
        toast.success('You product will find customer')
    };

    return (
        <>
            <Typography variant="h4">New Forum</Typography>
            <Form
                onSubmit={onSubmit}
                categories={categories}
            />
        </>
    );
};

export default NewOutput;