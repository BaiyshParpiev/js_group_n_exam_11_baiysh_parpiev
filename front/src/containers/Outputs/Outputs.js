import React, {useEffect} from 'react';
import {CircularProgress, Grid, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchOutputs} from "../../store/actions/outputsActions";
import OutputItem from '../../components/OutputItem/OutputItem';

const Outputs = () => {
    const dispatch = useDispatch();
    const {outputs, fetchLoading} = useSelector(state => state.outputs);

    useEffect(() => {
        dispatch(fetchOutputs());
    }, [dispatch]);

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justifyContent="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4">Outputs</Typography>
                </Grid>
            </Grid>
            <Grid item>
                <Grid item container direction="row" spacing={1}>
                    {fetchLoading ? (
                        <Grid container justifyContent="center" alignItems="center">
                            <Grid item>
                                <CircularProgress/>
                            </Grid>
                        </Grid>
                    ) : outputs.map(o => (
                        <OutputItem
                            key={o._id}
                            id={o._id}
                            title={o.title}
                            description={o.description}
                            image={o.image}
                            price={o.price}
                        />
                    ))}
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Outputs;