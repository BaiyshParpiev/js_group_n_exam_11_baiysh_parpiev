import React from 'react';
import {Link} from "react-router-dom";
import {Button} from "@material-ui/core";

const AnonymousMenu = () => {
  return (
    <>
      <Button component={Link} to="/register" color="inherit">Register</Button>
        <span> Or </span>
      <Button component={Link} to="/login" color="inherit">Sign in</Button>
    </>
  );
};

export default AnonymousMenu;