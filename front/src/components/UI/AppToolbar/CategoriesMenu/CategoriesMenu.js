import React, {useEffect, useState} from 'react';
import {Button, FormControl, makeStyles, MenuItem, Select} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchCategories} from "../../../../store/actions/categoriesActions";
import {fetchOutputs, fetchOutputsCategories} from "../../../../store/actions/outputsActions";

const useStyles = makeStyles((theme) => ({
    button: {
        display: 'block',
        marginTop: theme.spacing(2),
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
}));

const CategoriesMenu = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const {categories} = useSelector(state => state.categories);
    const [category, setCategory] = useState('');
    const [open, setOpen] = useState(false);

    const handleChange = (event) => {
        setCategory(event.target.value);
    };

    useEffect(() => {
        dispatch(fetchCategories());
    }, [dispatch]);

    const handleClose = () => {
        setOpen(false);
    };

    const handleOpen = () => {
        setOpen(true);
    };

    return (
        <div>
            <Button className={classes.button} onClick={handleOpen}>
                Category
            </Button>
            <FormControl className={classes.formControl}>
                <Select
                    labelId="demo-controlled-open-select-label"
                    id="demo-controlled-open-select"
                    open={open}
                    onClose={handleClose}
                    onOpen={handleOpen}
                    value={category}
                    onChange={handleChange}
                >
                    <MenuItem value={category} onClick={() => dispatch(fetchOutputs())}>All</MenuItem>
                    {categories && categories.map(c => (
                        <MenuItem key={c._id}
                                  value={category}
                                  onClick={() => dispatch(fetchOutputsCategories(c._id))}
                                  onChange={() => setCategory({category: c})}
                        >{c.title}</MenuItem>
                    ))}
                </Select>
            </FormControl>
        </div>
    );
};

export default CategoriesMenu;




