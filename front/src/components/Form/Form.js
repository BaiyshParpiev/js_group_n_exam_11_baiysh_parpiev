import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Input from "./Input/Input";
import {MenuItem} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2),
        backgroundColor: "white"
    }
}));

const Form = ({onSubmit, categories}) => {
    const classes = useStyles();

    const [state, setState] = useState({
        title: "",
        description: "",
        image: null,
        category: "",
        price: "",
    });

    const submitFormHandler = e => {
        e.preventDefault();


        const formData = new FormData();

        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });

        onSubmit(formData);
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prev => {
            return {...prev, [name]: file}
        });
    }

    return (
        <Grid
            container
            direction="column"
            spacing={2}
            component='form'
            className={classes.root}
            autoComplete='off'
            onSubmit={submitFormHandler}>
            <Grid item xs>
                <TextField
                    required
                    label="Title"
                    value={state.title}
                    onChange={inputChangeHandler}
                    name="title"
                    variant='outlined'
                    fullWidth
                />
            </Grid>
            <Grid item xs>
                <TextField
                    fullWidth
                    required
                    select
                    variant="outlined"
                    label="Category"
                    name="category"
                    value={state.category}
                    onChange={inputChangeHandler}
                >
                    <MenuItem>Select a category</MenuItem>
                    {categories.map(category => (
                        <MenuItem
                            key={category._id}
                            value={category._id}
                        >
                            {category.title}
                        </MenuItem>
                    ))}
                </TextField>
            </Grid>
            <Grid item xs>
                <TextField
                    required
                    multiline
                    rows={3}
                    type='text'
                    label="Description"
                    value={state.description}
                    onChange={inputChangeHandler}
                    name="description"
                    variant='outlined'
                    fullWidth
                />
            </Grid>
            <Grid item xs>
                <TextField
                    required
                    multiline
                    rows={3}
                    type='number'
                    label="Price"
                    value={state.price}
                    onChange={inputChangeHandler}
                    name="price"
                    variant='outlined'
                    fullWidth
                />
            </Grid>
            <Grid item xs>
                <Input
                    name="image"
                    label='Image'
                    onChange={fileChangeHandler}
                />
            </Grid>
            <Grid item xs>
                <Button type="submit" color="primary" disabled={!state.title} variant="contained">Create</Button>
            </Grid>
        </Grid>
    );
};

export default Form;
