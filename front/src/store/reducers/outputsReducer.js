import {
    DELETE_OUTPUT_FAILURE, DELETE_OUTPUT_REQUEST, DELETE_OUTPUT_SUCCESS,
    FETCH_OUTPUT_FAILURE,
    FETCH_OUTPUT_REQUEST, FETCH_OUTPUT_SUCCESS,
    FETCH_OUTPUTS_FAILURE,
    FETCH_OUTPUTS_REQUEST,
    FETCH_OUTPUTS_SUCCESS
} from "../actions/outputsActions";

const initialState = {
    fetchLoading: false,
    singleLoading: false,
    fetchError: null,
    outputs: [],
    output: null
};

const outputsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_OUTPUTS_REQUEST:
            return {...state, fetchLoading: true, fetchError: null,};
        case FETCH_OUTPUTS_SUCCESS:
            return {...state,  fetchLoading: false, outputs: action.payload, fetchError: null,};
        case FETCH_OUTPUTS_FAILURE:
            return {...state, fetchLoading: false, fetchError: action.payload};
        case FETCH_OUTPUT_REQUEST:
            return {...state, singleLoading: true, fetchError: null};
        case FETCH_OUTPUT_SUCCESS:
            return {...state,  singleLoading: false, output: action.payload, fetchError: null};
        case FETCH_OUTPUT_FAILURE:
            return {...state, singleLoading: false, fetchError: action.payload};
        case DELETE_OUTPUT_REQUEST:
            return {...state, singleLoading: true, fetchError: null};
        case DELETE_OUTPUT_SUCCESS:
            return {
                ...state,
                singleLoading: false,
                fetchError: null,
                outputs: state.outputs.filter(p => p._id !== action.payload),
            };
        case DELETE_OUTPUT_FAILURE:
            return {...state, singleLoading: false, fetchError: action.payload};
        default:
            return state;
    }
};

export default outputsReducer;