import {toast} from "react-toastify";
import {axiosApi} from "../../config";

export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';

export const LOGIN_USER_REQUEST = 'LOGIN_USER_REQUEST';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';

export const LOGOUT_USER = 'LOGOUT_USER';

export const registerUserSuccess = user => ({type: REGISTER_USER_SUCCESS, payload: user});
export const registerUserFailure = error => ({type: REGISTER_USER_FAILURE, payload: error});

export const loginUserRequest = () => ({type: LOGIN_USER_REQUEST});
export const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, payload: user});
export const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, payload: error});

export const registerUser = (userData, history) => {
  return async dispatch => {
    try {
      const response = await axiosApi.post('/users', userData);
        dispatch(registerUserSuccess(response.data));
        dispatch(history.push('/'));
        toast.success(`Welcome to Flea Market ${response.data.displayName}`)
    } catch (error) {
      if (error.response && error.response.data) {
        dispatch(registerUserFailure(error.response.data));
        toast.warning(error.response.data)
      } else {
        dispatch(registerUserFailure({global: 'No internet'}));
        toast.warning('No internet')
      }
    }
  };
};

export const loginUser = (userData, history) => {
  return async dispatch => {
    try {
      dispatch(loginUserRequest());
      const response = await axiosApi.post('/users/sessions', userData);
      dispatch(loginUserSuccess(response.data.user));
      dispatch(history.push('/'));
      toast.success(`Hi again ${response.data.displayName}`)
    } catch (error) {
      if (error.response && error.response.data) {
        dispatch(loginUserFailure(error.response.data));
        toast.warning(error.response.data)
      } else {
        dispatch(loginUserFailure({global: 'No internet'}));
      }
    }
  };
};

export const logoutUser = history => {
  return async (dispatch) => {
    try{
      await axiosApi.delete('/users/sessions');
      dispatch({type: LOGOUT_USER});
      history.push('/');
      toast.success('Logout successful')
    }catch{
      toast.error('Logout was not successful')
    }
  };
};
