import {toast} from "react-toastify";
import WarningIcon from '@material-ui/icons/Warning';
import {axiosApi} from "../../config";

export const FETCH_OUTPUTS_REQUEST = 'FETCH_OUTPUTS_REQUEST';
export const FETCH_OUTPUTS_SUCCESS = 'FETCH_OUTPUTS_SUCCESS';
export const FETCH_OUTPUTS_FAILURE = 'FETCH_OUTPUTS_FAILURE';

export const FETCH_OUTPUT_REQUEST = 'FETCH_OUTPUT_REQUEST';
export const FETCH_OUTPUT_SUCCESS = 'FETCH_OUTPUT_SUCCESS';
export const FETCH_OUTPUT_FAILURE = 'FETCH_OUTPUT_FAILURE';

export const CREATE_OUTPUT_REQUEST = 'CREATE_OUTPUT_REQUEST';
export const CREATE_OUTPUT_SUCCESS = 'CREATE_OUTPUT_SUCCESS';
export const CREATE_OUTPUT_FAILURE = 'CREATE_OUTPUT_FAILURE';

export const DELETE_OUTPUT_REQUEST = 'DELETE_OUTPUT_REQUEST';
export const DELETE_OUTPUT_SUCCESS = 'DELETE_OUTPUT_SUCCESS';
export const DELETE_OUTPUT_FAILURE = 'DELETE_OUTPUT_FAILURE';

export const fetchOutputsRequest = () => ({type: FETCH_OUTPUTS_REQUEST});
export const fetchOutputsSuccess = d => ({type: FETCH_OUTPUTS_SUCCESS, payload: d});
export const fetchOutputsFailure = () => ({type: FETCH_OUTPUTS_FAILURE});

export const fetchOutputRequest = () => ({type: FETCH_OUTPUT_REQUEST});
export const fetchOutputSuccess = d => ({type: FETCH_OUTPUT_SUCCESS, payload: d});
export const fetchOutputFailure = () => ({type: FETCH_OUTPUT_FAILURE});

export const createOutputRequest = () => ({type: CREATE_OUTPUT_REQUEST});
export const createOutputSuccess = () => ({type: CREATE_OUTPUT_SUCCESS});
export const createOutputFailure = () => ({type: CREATE_OUTPUT_FAILURE});

const deleteOutputRequest = () => ({type: DELETE_OUTPUT_REQUEST});
const deleteOutputSuccess = id => ({type: DELETE_OUTPUT_SUCCESS, payload: id});
const deleteOutputFailure = error => ({type: DELETE_OUTPUT_FAILURE, payload: error});


export const fetchOutputs = () => {
    return async (dispatch) => {
        try {
            dispatch(fetchOutputsRequest());
            const response = await axiosApi.get('/outputs' );
            dispatch(fetchOutputsSuccess(response.data));
            toast.success('All outputs.')
        } catch (error) {
            dispatch(fetchOutputsFailure(error));
            toast.error('Could not fetch products!', {
                theme: 'colored',
                icon: <WarningIcon/>
            });
        }
    };
};

export const fetchOutputsCategories = (id) => {
    return async (dispatch) => {
        try {
            dispatch(fetchOutputsRequest());
            const response = await axiosApi.get('/outputs?category=' + id);
            dispatch(fetchOutputsSuccess(response.data));
            toast.success(`Outputs with category ${response.data.category.title}`)
        } catch (error) {
            dispatch(fetchOutputsFailure());
        }
    };
};

export const fetchOutput = id => {
    return async dispatch => {
        try {
            dispatch(fetchOutputRequest());
            const response = await axiosApi.get('/outputs/' + id);
            dispatch(fetchOutputSuccess(response.data));
        } catch (e) {
            dispatch(fetchOutputFailure());
        }
    };
};

export const createOutput = d => {
    return async dispatch => {
        try {
            dispatch(createOutputRequest());
            const {data} = await axiosApi.post('/outputs', d);
            dispatch(createOutputSuccess(data));
            toast.success('Successful created');
        } catch (e) {
            dispatch(createOutputFailure());
            toast.error(e.response?.data?.message || e.response?.data?.error);
            throw e;
        }
    };
};

export const deleteOutput = (id, history) => async dispatch => {
    try{
        dispatch(deleteOutputRequest());
        const {data} = await axiosApi.delete('/outputs/' + id);
        dispatch(deleteOutputSuccess(data));
        toast.success(data.message)
        history.push('/');
    }catch(e){
        dispatch(deleteOutputFailure(e));
        if(e.response.status === 403){
            toast.error(e.response.data?.message)
        }
    }
};