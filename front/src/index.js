import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";
import {ToastContainer} from "react-toastify";
import App from './App';
import store from "./store/configureStore";
import 'react-toastify/dist/ReactToastify.css';
import {BrowserRouter} from "react-router-dom";

const app = (
    <Provider store={store}>
        <BrowserRouter>
                <ToastContainer/>
                <App/>
        </BrowserRouter>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));