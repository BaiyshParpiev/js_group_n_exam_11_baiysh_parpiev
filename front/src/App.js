import React from 'react';
import {Route, Switch} from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Outputs from "./containers/Outputs/Outputs";
import Output from "./containers/Output/Output";
import NewOutput from "./containers/NewOutput/NewOutput";

const App = () => {
    return (
        <Layout>
            <Switch>
                <Route path="/"  exact component={Outputs}/>
                <Route path="/outputs/:id"  component={Output}/>
                <Route path="/newOutput" component={NewOutput}/>
                <Route path="/register"  component={Register}/>
                <Route path="/login" component={Login}/>
            </Switch>
        </Layout>
    );
};

export default App;