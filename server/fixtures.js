const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const Category = require("./models/Category");
const User = require("./models/User");
const Output = require("./models/Output");

const run = async () => {
  await mongoose.connect(config.db.url);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [computers, products, home, others] = await Category.create({
    title: 'Computers',
    description: 'All different brands',
  },
    {
    title: 'Products',
    description: 'Vegetables, milk products',
  },
    {
    title: 'Home',
    description: 'Waters, balls',
  },
    {
    title: 'Other',
    description: 'Other what we need',
  });


  const [firstName, secondName] = await User.create({
    username: 'admin',
    password: 'admin',
    phoneNumber: 778533246,
    displayName: 'admin',
    token: nanoid(),
  }, {
    username: 'root',
    password: 'root',
    phoneNumber: 778533246,
    displayName: 'root',
    token: nanoid(),
  });

  await Output.create({
    title: "CPP",
    description: 'Last technology',
    author: firstName,
    image: 'fixtures/hdd.jpg',
    category: computers,
    price: 130
  }, {
    title: "Vegetables",
    description: 'Fresh vegetables from Osh',
    author: secondName,
    image: 'fixtures/gurke.jpeg',
    category: products,
    price: 130
  })



  await mongoose.connection.close();
};

run().catch(console.error);