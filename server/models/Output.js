const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const OutputSchema = new mongoose.Schema({
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Category',
    required: true,
  },
  price: {
    type: Number,
    min: 0,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  image: {
    type: String,
    required: true,
  },
});

OutputSchema.plugin(idValidator);

const Output = mongoose.model('Output', OutputSchema);

module.exports = Output;