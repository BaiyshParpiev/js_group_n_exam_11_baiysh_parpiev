const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Output = require('../models/Output');
const auth = require("../middleware/auth");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const query = {};

    if (req.query.category) {
      query.category = req.query.category;
    }

    const outputs = await Output.find(query).populate('category', 'title description');
    res.send(outputs);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const output = await Output.findById(req.params.id)
      .populate('author', 'displayName phoneNumber')
      .populate('category', 'title');

    if (output) {
      res.send(output);
    } else {
      res.status(404).send({error: 'Output not found'});
    }
  } catch {
    res.sendStatus(500);
  }
});

router.post('/', [auth, upload.single('image')], async (req, res) => {
  if(req.body.price < 0) return res.status(400).send({message: "Minimum price 0"})
  try {
    const outputData = {
      title: req.body.title,
      price: req.body.price,
      category: req.body.category,
      author: req.user._id,
      description: req.body.description,
    };

    if (req.file) {
      outputData.image = 'uploads/' + req.file.filename;
    }

    const output = new Output(outputData);
    await output.save();
    res.send(output);
  } catch {
    res.status(400).send({error: 'Data not valid'});
  }
});

router.delete('/:id', auth, async (req, res) => {
  const isMatch = await Output.findById(req.params.id);

  if (isMatch) {
    if (!req.user._id.equals(isMatch.author)) return res.status(403).send({message: "It is not your product"});
    const output = await Output.findByIdAndDelete(req.params.id);
    res.send({message: `Output '${output.title} removed'`});
  } else {
    res.status(404).send({error: 'Output not found'});
  }

});

module.exports = router;